var gulp = require('gulp');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var panini = require('panini');
var watch = require('gulp-watch');
var sync = require('browser-sync').create();



gulp.task('pages', function() {
  return gulp.src('src/pages/**/*.html')
    .pipe(panini({
      root: 'src/pages/',
      layouts: 'src/layouts/',
      partials: 'src/partials/',
      data: ['src/data/', 'src/partials/**/']
    }))
    .pipe(gulp.dest('public'));
});

gulp.task('reset', function() {
    panini.refresh();
    gulp.run('pages');
});

gulp.task('less', function() {
  return gulp.src('src/assets/less/main.less')
    .pipe(less())
    .pipe(autoprefixer({browsers: ['last 10 versions']}))
    .pipe(gulp.dest('public/assets/css'))
    .pipe(sync.stream());
});

gulp.task('copy', function() {
  gulp.src(['src/assets/**/*', '!src/assets/less/*'])
    .pipe(gulp.dest('public/assets'))
    .pipe(sync.stream());
});


gulp.task('default', function() {

  sync.init({
    logConnections: true,
    server: ['public'],
    open: true,
    port: 3000
  });

  gulp.watch(['src/assets/**/*'], ['copy']);
  gulp.watch(['src/assets/less/*.less', 'src/partials/**/*.less'], ['less']);
  gulp.watch(['src/{layouts,partials,pages,helpers,data}/**/*'], ['reset']);
  gulp.watch(['src/{layouts,partials,pages,helpers,data}/**/*.html'], [sync.reload]);
});
